package warpgate

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	AlaudaFeatureGateGroup           = "alauda.io"
	AlaudaFeatureGateAPIVersion      = "alauda.io/v1"
	AlaudaFeatureGateKind            = "AlaudaFeatureGate"
	AlaudaFeatureGateListKind        = "AlaudaFeatureGateList"
	AlaudaFeatureGateResource        = "alaudafeaturegates"
	ClusterAlaudaFeatureGateKind     = "ClusterAlaudaFeatureGate"
	ClusterAlaudaFeatureGateResource = "clusteralaudafeaturegates"
)

type AlaudaFeatureGateStage string

const (
	Alpha AlaudaFeatureGateStage = "Alpha"
	Beta  AlaudaFeatureGateStage = "Beta"
	GA    AlaudaFeatureGateStage = "GA"
	EOF   AlaudaFeatureGateStage = "EOF"
)

const (
	AlphaFeaturesGateName = "alpha"
	BetaFeaturesGateName  = "beta"
	EOFFeaturesGateName   = "eof"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// AlaudaFeatureGate describes a feature gate of the alauda product.
// +k8s:openapi-gen=true
type AlaudaFeatureGate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AlaudaFeatureGateSpec   `json:"spec,omitempty"`
	Status AlaudaFeatureGateStatus `json:"status,omitempty"`
}

// AlaudaFeatureGateSpec defines the desired state of AlaudaFeatureGate.
type AlaudaFeatureGateSpec struct {
	// Description is a human readable text of the feature gate.
	Description string `json:"description"`

	// Stage is the stage of the feature gate, should be one of "Alpha", "Beta", "GA" or "EOF".
	Stage AlaudaFeatureGateStage `json:"stage"`

	// Dependency is the denpendencies of the feature gate.
	Dependency AlaudaFeatureGateDependency `json:"dependency,omitempty"`

	// Enabled indicates if the feature gate is enabled manually.
	Enabled bool `json:"enabled"`
}

// AlaudaFeatureGateStatus defines the observed state of AlaudaFeatureGate
type AlaudaFeatureGateStatus struct {
	// Enabled indicates if the feature gate is enabled at runtime.
	Enabled bool `json:"enabled"`
}

type AlaudaFeatureGateDependencyType string

const (
	AnyDenpendency   AlaudaFeatureGateDependencyType = "any"
	AllDenpendencies                                 = "all"
)

// AlaudaFeatureGateDependency describes the dependency of a feature gate.
type AlaudaFeatureGateDependency struct {
	// FeatureGates is the names of feature gates that depends on.
	FeatureGates []string `json:"featureGates,omitempty"`
	// Type is the type of dependency, should be one of "all" or "any".
	Type AlaudaFeatureGateDependencyType `json:"type,omitempty"`
}

// AlaudaFeatureGateList is a list of AlaudaFeatureGate.
type AlaudaFeatureGateList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []AlaudaFeatureGate `json:"items"`
}
