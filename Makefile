PKG := alauda.io/warpgate

PACKAGES := $(shell go list ./... | grep -v /vendor/)

GOFILES := $(shell find . -name "*.go" -type f -not -path "./vendor/*")

LINTFILES := $(shell find . -name "*.go" -type f -not -path "./alaudafeaturegate_types.go")

GOFMT ?= gofmt "-s"

.PHONY: setup ci check fmt lint test

all: check test

check: setup lint fmt-check

setup:
	@go get -u golang.org/x/lint/golint

.PHONY: fmt
fmt:
	@$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check:
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

lint:
	@test -z "$$(golint $(LINTFILES) | tee /dev/stderr)"

test:
	@mkdir -p ./output
	go test -v -cover -coverprofile ./output/coverage.out $(PACKAGES) && go tool cover -func ./output/coverage.out || exit 1
