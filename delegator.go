package warpgate

import (
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	restclient "k8s.io/client-go/rest"
	"k8s.io/klog"
	"strings"
)

const (
	// FeatureGateListURLTemplate is the API URL template to access global feature gate list.
	FeatureGateListURLTemplate = "%s/fg/v1/featuregates"
	// FeatureGateURLTemplate is the API URL template to access a global feature gate.
	FeatureGateURLTemplate = "%s/fg/v1/featuregates/%s"
	// LicenseFeatureGateURLTemplate is the API URL template to access license feature gate.
	LicenseFeatureGateURLTemplate = "%s/fg/v1/featuregates/license?product=%s"
	// ClusterFeatureGateListURLTemplate is the API URL template to access cluster feature gate list.
	ClusterFeatureGateListURLTemplate = "%s/fg/v1/%s/featuregates"
	// ClusterFeatureGateURLTemplate is the API URL template to access a cluster feature gate.
	ClusterFeatureGateURLTemplate = "%s/fg/v1/%s/featuregates/%s"

	// LocalFeatureGateNamespace is the namespace of the global feature gate in local cluster.
	LocalFeatureGateNamespace = "kube-public"
)

// InClusterConfigFunc returns the rest config of the k8s cluster where the component is running.
type InClusterConfigFunc func() (*restclient.Config, error)

var inClusterConfig InClusterConfigFunc = restclient.InClusterConfig

// FeatureGateDelegator accesses the feature gate resources.
type FeatureGateDelegator interface {
	// FeatureGates returns an AlaudaFeatureGateList.
	FeatureGates() (*AlaudaFeatureGateList, error)
	// FeatureGate returns an AlaudaFeatureGate by name.
	FeatureGate(name string) (*AlaudaFeatureGate, error)
	// ClusterFeatureGates returns an AlaudaFeatureGateList of a cluster.
	ClusterFeatureGates(cluster string) (*AlaudaFeatureGateList, error)
	// ClusterFeatureGate returns an AlaudaFeatureGate of a cluster by name.
	ClusterFeatureGate(cluster, name string) (*AlaudaFeatureGate, error)
}

// GetFeatureGateDelegator returns a FeatureGateDelegator instance by the Config.
func GetFeatureGateDelegator(config Config) (FeatureGateDelegator, error) {
	if config.APIEndpoint == "" {
		// Use local cluster resources to resolve feature gates
		return NewLocalFeatureGateDelegator()
	}

	// Use high level APIs to resolve feature gates
	return NewRemoteFeatureGateDelegator(config), nil
}

// RemoteFeatureGateDelegator implements the FeatureGateDelegator for remote feature gates access in the global cluster.
type RemoteFeatureGateDelegator struct {
	config Config
}

// NewRemoteFeatureGateDelegator returns a new RemoteFeatureGateDelegator instance.
func NewRemoteFeatureGateDelegator(config Config) *RemoteFeatureGateDelegator {
	return &RemoteFeatureGateDelegator{config: config}
}

// FeatureGates returns an AlaudaFeatureGateList.
func (r *RemoteFeatureGateDelegator) FeatureGates() (fglist *AlaudaFeatureGateList, err error) {
	url := fmt.Sprintf(FeatureGateListURLTemplate, r.config.APIEndpoint)
	fglist = &AlaudaFeatureGateList{}
	err = QueryURLForObject(url, r.config.AuthorizationToken, fglist)
	if err != nil {
		fglist = nil
	}
	return
}

// FeatureGate returns an AlaudaFeatureGate by name.
func (r *RemoteFeatureGateDelegator) FeatureGate(name string) (fg *AlaudaFeatureGate, err error) {
	url := fmt.Sprintf(FeatureGateURLTemplate, r.config.APIEndpoint, name)
	fg = &AlaudaFeatureGate{}
	err = QueryURLForObject(url, r.config.AuthorizationToken, fg)
	if err != nil {
		fg = nil
	}
	return
}

// ClusterFeatureGates returns an AlaudaFeatureGateList of a cluster.
func (r *RemoteFeatureGateDelegator) ClusterFeatureGates(cluster string) (fglist *AlaudaFeatureGateList, err error) {
	url := fmt.Sprintf(ClusterFeatureGateListURLTemplate, r.config.APIEndpoint, cluster)
	fglist = &AlaudaFeatureGateList{}
	err = QueryURLForObject(url, r.config.AuthorizationToken, fglist)
	if err != nil {
		fglist = nil
	}
	return
}

// ClusterFeatureGate returns an AlaudaFeatureGate of a cluster by name.
func (r *RemoteFeatureGateDelegator) ClusterFeatureGate(cluster, name string) (fg *AlaudaFeatureGate, err error) {
	url := fmt.Sprintf(ClusterFeatureGateURLTemplate, r.config.APIEndpoint, cluster, name)
	fg = &AlaudaFeatureGate{}
	err = QueryURLForObject(url, r.config.AuthorizationToken, fg)
	if err != nil {
		fg = nil
	}
	return
}

// LocalFeatureGateDelegator implements FeatureGateDelegator for feature gates access in local cluster.
type LocalFeatureGateDelegator struct {
	clusterFeatureNamespace string
	globalFeatureNamespace  string
	client                  dynamic.Interface
}

// NewLocalFeatureGateDelegator returns a new LocalFeatureGateDelegator instance.
func NewLocalFeatureGateDelegator() (*LocalFeatureGateDelegator, error) {
	cfg, err := inClusterConfig()
	if err != nil {
		return nil, err
	}
	client, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	return &LocalFeatureGateDelegator{
		clusterFeatureNamespace: PodNamespace(),
		globalFeatureNamespace:  LocalFeatureGateNamespace,
		client:                  client,
	}, nil
}

func (l *LocalFeatureGateDelegator) resolveFeatureGates() (fgs *AlaudaFeatureGateList, err error) {
	ri := l.client.Resource(schema.GroupVersionResource{
		Group:    AlaudaFeatureGateGroup,
		Version:  "v1",
		Resource: AlaudaFeatureGateResource,
	}).Namespace(l.globalFeatureNamespace)

	features, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return
	}

	ftMap := make(map[string]*unstructured.Unstructured)
	for _, f := range features.Items {
		f := f
		ftMap[f.GetName()] = &f
	}

	ri = l.client.Resource(schema.GroupVersionResource{
		Group:    AlaudaFeatureGateGroup,
		Version:  "v1",
		Resource: ClusterAlaudaFeatureGateResource,
	}).Namespace(l.clusterFeatureNamespace)

	features, err = ri.List(metav1.ListOptions{})
	if err != nil {
		return
	}

	for _, f := range features.Items {
		f := f
		ftMap[f.GetName()] = &f
	}

	fgs = &AlaudaFeatureGateList{
		Items: []AlaudaFeatureGate{},
	}
	fgs.APIVersion = AlaudaFeatureGateAPIVersion
	fgs.Kind = AlaudaFeatureGateListKind

	resolved := make(map[string]bool)
	for name, f := range ftMap {
		fObj, errI := UnstructuredToAlaudaFeatureGate(f)
		if errI != nil {
			err = errI
			return
		}

		_, ok := resolved[name]
		if !ok {
			enabled, err := ResolveFeatureGateState(fObj, ftMap, make(map[string]bool), resolved)
			if err != nil {
				klog.Warningf("resolve feature gate error: %v, assume the feature gate is disabled", err)
			}
			resolved[name] = enabled
		}

		fObj.Status.Enabled = resolved[name]
		fgs.Items = append(fgs.Items, *fObj)
	}
	return
}

func (l *LocalFeatureGateDelegator) resolve() (*AlaudaFeatureGateList, error) {
	return l.resolveFeatureGates()
}

// FeatureGates returns an AlaudaFeatureGateList.
func (l *LocalFeatureGateDelegator) FeatureGates() (*AlaudaFeatureGateList, error) {
	return l.resolve()
}

// FeatureGate returns an AlaudaFeatureGate by name.
func (l *LocalFeatureGateDelegator) FeatureGate(name string) (*AlaudaFeatureGate, error) {
	fgs, err := l.resolve()
	if err != nil {
		return nil, err
	}
	for _, fg := range fgs.Items {
		if fg.Name == name {
			return &fg, nil
		}
	}
	return nil, fmt.Errorf("feature gate %q not found", name)
}

// ClusterFeatureGates returns an AlaudaFeatureGateList of a cluster.
func (l *LocalFeatureGateDelegator) ClusterFeatureGates(cluster string) (*AlaudaFeatureGateList, error) {
	return l.FeatureGates()
}

// ClusterFeatureGate returns an AlaudaFeatureGate of a cluster by name.
func (l *LocalFeatureGateDelegator) ClusterFeatureGate(cluster string, name string) (*AlaudaFeatureGate, error) {
	return l.FeatureGate(name)
}

// ResolveFeatureGateState resolves the feature gate's enabled state by
// resolving its dependencies and global feature stage gate recursively.
//
// The dependency graph should not have circles, an error will return if it
// has one.
//
// Parameters:
//
// - feature: The feature gate to be resolved.
//
// - features: The registered feature list.
//
// - visited: The visited feature map used to detect the dependency circle. An empty map should be passed on the initial method call.
//
// - resolved: A map stores the feature name as the key, and it's resolved enabled state as value.
//
// It returns feature's enabled state and error encountered.
func ResolveFeatureGateState(
	feature *AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	defer func() {
		if err == nil && enabled {
			resolved[feature.Name] = true
		} else {
			resolved[feature.Name] = false
		}
	}()

	if visited[feature.Name] {
		return false, errors.NewInternalError(fmt.Errorf("feature dependency circle detected at %q", feature.Name))
	}

	if _, ok := resolved[feature.Name]; ok {
		return resolved[feature.Name], nil
	}

	// If the stage feature gate is disabled, the feature gate is disabled
	// in spite of its 'enabled' value.
	featureStage := features[strings.ToLower(string(feature.Spec.Stage))]
	if featureStage != nil {
		fgObj, err := UnstructuredToAlaudaFeatureGate(featureStage)
		if err != nil {
			return false, err
		}
		if !fgObj.Spec.Enabled {
			return false, nil
		}
	}

	if !feature.Spec.Enabled {
		return false, nil
	}

	if len(feature.Spec.Dependency.FeatureGates) == 0 {
		return feature.Spec.Enabled, nil
	}

	visited[feature.Name] = true
	enabled = true

	switch feature.Spec.Dependency.Type {
	case AllDenpendencies:
		enabled, err = resolveAllDependency(feature, features, visited, resolved)
	case AnyDenpendency:
		enabled, err = resolveAnyDependency(feature, features, visited, resolved)
	default:
		enabled = false
		err = errors.NewInternalError(fmt.Errorf("invalid feature dependency type %q", feature.Spec.Dependency.Type))
	}
	visited[feature.Name] = false
	return
}

func resolveAllDependency(
	feature *AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	for _, f := range feature.Spec.Dependency.FeatureGates {
		fg := features[f]
		if fg != nil {
			fgObj, errInternal := UnstructuredToAlaudaFeatureGate(fg)
			if errInternal != nil {
				enabled = false
				err = errInternal
				break
			}
			enabled, err = ResolveFeatureGateState(fgObj, features, visited, resolved)
			if err != nil || !enabled {
				break
			}
		} else {
			enabled = false
			err = errors.NewInternalError(fmt.Errorf("feature %q not found", f))
			break
		}
	}
	return enabled, err
}

func resolveAnyDependency(
	feature *AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	for _, f := range feature.Spec.Dependency.FeatureGates {
		fg := features[f]
		if fg != nil {
			fgObj, errInternal := UnstructuredToAlaudaFeatureGate(fg)
			if errInternal != nil {
				enabled = false
				err = errInternal
				break
			}
			enabled, err = ResolveFeatureGateState(fgObj, features, visited, resolved)
			if err == nil && enabled {
				break
			}
		} else {
			continue
		}
	}
	return enabled, err
}
