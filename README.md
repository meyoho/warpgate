# WarpGate

Warpgate is a golang client library to access alauda feature gate API easily.

## Quick Start

### Add `warpgate` to your go module

Run the commands in your project root:

```shell script
$ go mod edit --require=alauda.io/warpgate@v0.0.1 
$ go mod edit --replace alauda.io/warpgate@v0.0.1=bitbucket.org/mathildetech/warpgate@v0.0.1
```
replace the `v0.0.1` with your desired version.

### Use `warpgate` in your code

```golang
import wg "alauda.io/warpgate"

func foo() {
	// In global cluster, create a warpgate by the cluster authorization bearer token
	// and feature gate api endpoint.
	wwg, _ := wg.NewWarpGate(wg.Config{
		AuthorizationToken: "xsop12.3xlopijkiop",
		APIEndpoint: "http://archon"})
	// In non-global clusters, create a local WarpGate
	// wwg, _ := wwg.NewLocalWarpGate()
	
	// Query if the feature gate `a` on cluster `foo` is enabled 
	enabled, _ := wwg.IsClusterFeatureGateEnabled("foo", "a")
	// Query if a product is licensed
	enabled = wwg.IsProductLicensed("Container-Platform")

}
```

For more useful functions, please refer to the godoc of the library.
