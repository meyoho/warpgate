package warpgate

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func fakeHTTPClientTransport(body string, statusCode int) RoundTripFunc {
	return func(req *http.Request) *http.Response {
		return &http.Response{
			StatusCode: statusCode,
			Body:       ioutil.NopCloser(bytes.NewBufferString(body)),
			Header:     make(http.Header),
		}
	}
}

func prepareFakeHTTPClient(body string, statusCode int) {
	client.Transport = fakeHTTPClientTransport(body, statusCode)
}

func TestAddAuthorizationBearerTokenHeader(t *testing.T) {
	type args struct {
		header *http.Header
		token  string
	}
	tests := []struct {
		name         string
		args         args
		expectHeader string
	}{
		{
			name: "ok",
			args: args{
				header: &http.Header{},
				token:  "foobar",
			},
			expectHeader: "Bearer foobar",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			AddAuthorizationBearerTokenHeader(tt.args.header, tt.args.token)
			if tt.args.header.Get("Authorization") != tt.expectHeader {
				t.Errorf("Expect Authorization header: %s, got %s", tt.expectHeader, tt.args.header.Get("Authorization"))
			}
		})
	}
}

func TestQueryURLForObject(t *testing.T) {
	type args struct {
		url   string
		token string
		obj   interface{}
	}
	tests := []struct {
		name    string
		args    args
		resp    string
		expect  interface{}
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				url:   "http://fake.com",
				token: "fake token",
				obj:   &AlaudaFeatureGate{},
			},
			resp: `
{
    "kind": "AlaudaFeatureGate",
    "spec": {
        "dependency": {
            "type": "all",
            "featureGates": [
                "b",
                "d"
            ]
        },
        "enabled": true,
        "description": "a",
        "stage": "Alpha"
    },
    "apiVersion": "alauda.io/v1",
    "metadata": {
        "namespace": "default",
        "name": "a"
    }
}
`,
			expect: &AlaudaFeatureGate{
				TypeMeta: metav1.TypeMeta{
					Kind:       "AlaudaFeatureGate",
					APIVersion: "alauda.io/v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "a",
					Namespace: "default",
				},
				Spec: AlaudaFeatureGateSpec{
					Dependency: AlaudaFeatureGateDependency{
						Type: "all",
						FeatureGates: []string{
							"b", "d",
						},
					},
					Enabled:     true,
					Description: "a",
					Stage:       "Alpha",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.resp, http.StatusOK)
			err := QueryURLForObject(tt.args.url, tt.args.token, tt.args.obj)
			if err != nil {
				if !tt.wantErr {
					t.Errorf("QueryURLForObject() error = %v, wantErr %v", err, tt.wantErr)
				}
			} else {
				if !reflect.DeepEqual(tt.args.obj, tt.expect) {
					t.Errorf("Expect obj %+v, got %+v", tt.expect, tt.args.obj)
				}
			}
		})
	}
}
