package warpgate

import (
	"reflect"
	"testing"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestNewWarpGate(t *testing.T) {
	type args struct {
		config Config
	}
	tests := []struct {
		name    string
		args    args
		want    *defaultWarpGate
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				config: Config{
					APIEndpoint:        "http://archon",
					AuthorizationToken: "foo",
				},
			},
			want: &defaultWarpGate{
				Config: Config{
					APIEndpoint:        "http://archon",
					AuthorizationToken: "foo",
				},
			},
		},
		{
			name: "with endpoint",
			args: args{
				config: Config{
					APIEndpoint:        "http://foo",
					AuthorizationToken: "foo",
				},
			},
			want: &defaultWarpGate{
				Config: Config{
					APIEndpoint:        "http://foo",
					AuthorizationToken: "foo",
				},
			},
		},
		{
			name: "error",
			args: args{
				config: Config{
					AuthorizationToken: "foo",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewWarpGate(tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewLocalWarpGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestNewLocalWarpGate(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name: "ok",
		},
		{
			name:    "error",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			inClusterConfig = fakeInClusterConfigFunc(tt.wantErr)
			got, err := NewLocalWarpGate()
			if (err != nil) != tt.wantErr {
				t.Errorf("NewLocalWarpGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && got == nil {
				t.Errorf("NewLocalWarpGate() got nil")
			}
		})
	}
}

func TestWarpGate_ListFeatureGates(t *testing.T) {
	tests := []struct {
		name         string
		responseBody string
		statusCode   int
		wantFglist   *AlaudaFeatureGateList
		wantErr      bool
	}{
		{
			name: "ok",
			responseBody: `
{
	"kind": "AlaudaFeatureGateList",
	"apiVersion": "alauda.io/v1",
	"items": [
		{
			"kind": "AlaudaFeatureGate",
			"spec": {
				"dependency": {
					"type": "all",
					"featureGates": [
						"b",
						"d"
					]
				},
				"enabled": true,
				"description": "a",
				"stage": "Alpha"
			},
			"apiVersion": "alauda.io/v1",
			"metadata": {
				"namespace": "default",
				"name": "a"
			},
			"status": {
				"enabled": true
			}
		},{
			"kind": "AlaudaFeatureGate",
			"spec": {
				"enabled": true,
				"description": "b",
				"stage": "Beta"
			},
			"apiVersion": "alauda.io/v1",
			"metadata": {
				"namespace": "default",
				"name": "b"
			},
			"status": {
				"enabled": false
			}
		}
	]
}
`,
			statusCode: 200,
			wantFglist: &AlaudaFeatureGateList{
				TypeMeta: metav1.TypeMeta{
					Kind:       "AlaudaFeatureGateList",
					APIVersion: "alauda.io/v1",
				},
				Items: []AlaudaFeatureGate{
					{
						TypeMeta: metav1.TypeMeta{
							Kind:       "AlaudaFeatureGate",
							APIVersion: "alauda.io/v1",
						},
						ObjectMeta: metav1.ObjectMeta{
							Name:      "a",
							Namespace: "default",
						},
						Spec: AlaudaFeatureGateSpec{
							Dependency: AlaudaFeatureGateDependency{
								Type: "all",
								FeatureGates: []string{
									"b", "d",
								},
							},
							Enabled:     true,
							Description: "a",
							Stage:       "Alpha",
						},
						Status: AlaudaFeatureGateStatus{
							Enabled: true,
						},
					}, {
						TypeMeta: metav1.TypeMeta{
							Kind:       "AlaudaFeatureGate",
							APIVersion: "alauda.io/v1",
						},
						ObjectMeta: metav1.ObjectMeta{
							Name:      "b",
							Namespace: "default",
						},
						Spec: AlaudaFeatureGateSpec{
							Enabled:     true,
							Description: "b",
							Stage:       "Beta",
						},
						Status: AlaudaFeatureGateStatus{
							Enabled: false,
						},
					},
				},
			},
		},
		{
			name:       "404",
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{APIEndpoint: "http://foo", AuthorizationToken: "foo"})
			gotFglist, err := f.ListFeatureGates()
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.ListFeatureGates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantFglist == nil {
				return
			}

			if !reflect.DeepEqual(gotFglist, tt.wantFglist) {
				t.Errorf("defaultWarpGate.ListFeatureGates() = %v, want %v", gotFglist, tt.wantFglist)
			}
		})
	}
}

func TestWarpGate_GetFeatureGate(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name         string
		responseBody string
		statusCode   int
		args         args
		wantFg       *AlaudaFeatureGate
		wantErr      bool
	}{
		{
			name: "ok",
			responseBody: `
{
	"kind": "AlaudaFeatureGate",
	"spec": {
		"dependency": {
			"type": "all",
			"featureGates": [
				"b",
				"d"
			]
		},
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "a"
	},
	"status": {
		"enabled": true
	}
}
`,
			statusCode: 200,
			args: args{
				name: "a",
			},
			wantFg: &AlaudaFeatureGate{
				TypeMeta: metav1.TypeMeta{
					Kind:       "AlaudaFeatureGate",
					APIVersion: "alauda.io/v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "a",
					Namespace: "default",
				},
				Spec: AlaudaFeatureGateSpec{
					Dependency: AlaudaFeatureGateDependency{
						Type: "all",
						FeatureGates: []string{
							"b", "d",
						},
					},
					Enabled:     true,
					Description: "a",
					Stage:       "Alpha",
				},
				Status: AlaudaFeatureGateStatus{
					Enabled: true,
				},
			},
		},
		{
			name:       "404",
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{
				APIEndpoint:        "http://foo",
				AuthorizationToken: "foo",
			})
			gotFg, err := f.GetFeatureGate(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.GetFeatureGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotFg, tt.wantFg) {
				t.Errorf("defaultWarpGate.GetFeatureGate() = %v, want %v", gotFg, tt.wantFg)
			}
		})
	}
}

func TestWarpGate_IsFeatureGateEnabled(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name         string
		args         args
		responseBody string
		statusCode   int
		wantEnabled  bool
		wantErr      bool
	}{
		{
			name: "ok",
			args: args{
				name: "a",
			},
			responseBody: `
{
	"kind": "AlaudaFeatureGate",
	"spec": {
		"dependency": {
			"type": "all",
			"featureGates": [
				"b",
				"d"
			]
		},
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "a"
	},
	"status": {
		"enabled": true
	}
}
`,
			statusCode:  200,
			wantEnabled: true,
		}, {
			name: "error",
			args: args{
				name: "a",
			},
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{
				APIEndpoint:        "http://foo",
				AuthorizationToken: "foo",
			})
			gotEnabled, err := f.IsFeatureGateEnabled(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.IsFeatureGateEnabled() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotEnabled != tt.wantEnabled {
				t.Errorf("defaultWarpGate.IsFeatureGateEnabled() = %v, want %v", gotEnabled, tt.wantEnabled)
			}
		})
	}
}

func TestWarpGate_ListClusterFeatureGates(t *testing.T) {
	type args struct {
		cluster string
	}
	tests := []struct {
		name         string
		args         args
		responseBody string
		statusCode   int
		wantFglist   *AlaudaFeatureGateList
		wantErr      bool
	}{
		{
			name: "ok",
			args: args{
				cluster: "foo",
			},
			responseBody: `
{
	"kind": "ClusterAlaudaFeatureGateList",
	"apiVersion": "alauda.io/v1",
	"items": [
		{
			"kind": "ClusterAlaudaFeatureGate",
			"spec": {
				"dependency": {
					"type": "all",
					"featureGates": [
						"b",
						"d"
					]
				},
				"enabled": true,
				"description": "a",
				"stage": "Alpha"
			},
			"apiVersion": "alauda.io/v1",
			"metadata": {
				"namespace": "default",
				"name": "a"
			},
			"status": {
				"enabled": true
			}
		},{
			"kind": "ClusterAlaudaFeatureGate",
			"spec": {
				"enabled": true,
				"description": "b",
				"stage": "Beta"
			},
			"apiVersion": "alauda.io/v1",
			"metadata": {
				"namespace": "default",
				"name": "b"
			},
			"status": {
				"enabled": false
			}
		}
	]
}
`,
			statusCode: 200,
			wantFglist: &AlaudaFeatureGateList{
				TypeMeta: metav1.TypeMeta{
					Kind:       "ClusterAlaudaFeatureGateList",
					APIVersion: "alauda.io/v1",
				},
				Items: []AlaudaFeatureGate{
					{
						TypeMeta: metav1.TypeMeta{
							Kind:       "ClusterAlaudaFeatureGate",
							APIVersion: "alauda.io/v1",
						},
						ObjectMeta: metav1.ObjectMeta{
							Name:      "a",
							Namespace: "default",
						},
						Spec: AlaudaFeatureGateSpec{
							Dependency: AlaudaFeatureGateDependency{
								Type: "all",
								FeatureGates: []string{
									"b", "d",
								},
							},
							Enabled:     true,
							Description: "a",
							Stage:       "Alpha",
						},
						Status: AlaudaFeatureGateStatus{
							Enabled: true,
						},
					}, {
						TypeMeta: metav1.TypeMeta{
							Kind:       "ClusterAlaudaFeatureGate",
							APIVersion: "alauda.io/v1",
						},
						ObjectMeta: metav1.ObjectMeta{
							Name:      "b",
							Namespace: "default",
						},
						Spec: AlaudaFeatureGateSpec{
							Enabled:     true,
							Description: "b",
							Stage:       "Beta",
						},
						Status: AlaudaFeatureGateStatus{
							Enabled: false,
						},
					},
				},
			},
		},
		{
			name:       "404",
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{
				APIEndpoint:        "http://foo",
				AuthorizationToken: "foo",
			})
			gotFglist, err := f.ListClusterFeatureGates(tt.args.cluster)
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.ListClusterFeatureGates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotFglist, tt.wantFglist) {
				t.Errorf("defaultWarpGate.ListClusterFeatureGates() = %v, want %v", gotFglist, tt.wantFglist)
			}
		})
	}
}

func TestWarpGate_GetClusterFeatureGate(t *testing.T) {
	type args struct {
		cluster string
		name    string
	}
	tests := []struct {
		name         string
		args         args
		responseBody string
		statusCode   int
		wantFg       *AlaudaFeatureGate
		wantErr      bool
	}{
		{
			name: "ok",
			responseBody: `
{
	"kind": "ClusterAlaudaFeatureGate",
	"spec": {
		"dependency": {
			"type": "all",
			"featureGates": [
				"b",
				"d"
			]
		},
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "a"
	},
	"status": {
		"enabled": true
	}
}
`,
			statusCode: 200,
			args: args{
				cluster: "foo",
				name:    "a",
			},
			wantFg: &AlaudaFeatureGate{
				TypeMeta: metav1.TypeMeta{
					Kind:       "ClusterAlaudaFeatureGate",
					APIVersion: "alauda.io/v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "a",
					Namespace: "default",
				},
				Spec: AlaudaFeatureGateSpec{
					Dependency: AlaudaFeatureGateDependency{
						Type: "all",
						FeatureGates: []string{
							"b", "d",
						},
					},
					Enabled:     true,
					Description: "a",
					Stage:       "Alpha",
				},
				Status: AlaudaFeatureGateStatus{
					Enabled: true,
				},
			},
		},
		{
			name:       "404",
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{
				APIEndpoint:        "http://foo",
				AuthorizationToken: "foo",
			})
			gotFg, err := f.GetClusterFeatureGate(tt.args.cluster, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.GetClusterFeatureGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotFg, tt.wantFg) {
				t.Errorf("defaultWarpGate.GetClusterFeatureGate() = %v, want %v", gotFg, tt.wantFg)
			}
		})
	}
}

func TestWarpGate_IsClusterFeatureGateEnabled(t *testing.T) {
	type args struct {
		cluster string
		name    string
	}
	tests := []struct {
		name         string
		args         args
		responseBody string
		statusCode   int
		wantEnabled  bool
		wantErr      bool
	}{
		{
			name: "ok",
			args: args{
				cluster: "foo",
				name:    "a",
			},
			responseBody: `
{
	"kind": "ClusterAlaudaFeatureGate",
	"spec": {
		"dependency": {
			"type": "all",
			"featureGates": [
				"b",
				"d"
			]
		},
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "a"
	},
	"status": {
		"enabled": true
	}
}
`,
			statusCode:  200,
			wantEnabled: true,
		}, {
			name: "error",
			args: args{
				cluster: "foo",
				name:    "a",
			},
			statusCode: 404,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f, _ := NewWarpGate(Config{
				APIEndpoint:        "http://foo",
				AuthorizationToken: "foo",
			})
			gotEnabled, err := f.IsClusterFeatureGateEnabled(tt.args.cluster, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("defaultWarpGate.IsClusterFeatureGateEnabled() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotEnabled != tt.wantEnabled {
				t.Errorf("defaultWarpGate.IsClusterFeatureGateEnabled() = %v, want %v", gotEnabled, tt.wantEnabled)
			}
		})
	}
}

func TestWarpGate_IsProductLicensed(t *testing.T) {
	tests := []struct {
		name         string
		product      string
		responseBody string
		statusCode   int
		wantEnabled  bool
	}{
		{
			name:    "enabled",
			product: "ACP",
			responseBody: `
{
	"kind": "AlaudaFeatureGate",
	"spec": {
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "License"
	},
	"status": {
		"enabled": true
	}
}
`,
			statusCode:  200,
			wantEnabled: true,
		}, {
			name:        "disabled 404",
			product:     "ACP",
			statusCode:  404,
			wantEnabled: false,
		}, {
			name:    "disabled",
			product: "ACP",
			responseBody: `
{
	"kind": "AlaudaFeatureGate",
	"spec": {
		"enabled": true,
		"description": "a",
		"stage": "Alpha"
	},
	"apiVersion": "alauda.io/v1",
	"metadata": {
		"namespace": "default",
		"name": "License"
	},
	"status": {
		"enabled": false
	}
}
`,
			statusCode:  200,
			wantEnabled: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			prepareFakeHTTPClient(tt.responseBody, tt.statusCode)
			f := &defaultWarpGate{}
			if gotEnabled := f.IsProductLicensed(tt.product); gotEnabled != tt.wantEnabled {
				t.Errorf("IsProductLicensed() = %v, want %v", gotEnabled, tt.wantEnabled)
			}
		})
	}
}
