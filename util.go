package warpgate

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	// EnvPodNamespace is the environment variable name of the namespace of a pod.
	EnvPodNamespace = "POD_NAMESPACE"
)

// AddAuthorizationBearerTokenHeader adds a authorization bearer token header to
// the http header.
func AddAuthorizationBearerTokenHeader(header *http.Header, token string) {
	header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
}

// QueryURLForObject queries the url with bearer token and unmarshal the response body into
// the input `obj` argument.
func QueryURLForObject(url, token string, obj interface{}) (err error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return
	}
	AddAuthorizationBearerTokenHeader(&req.Header, token)
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	var body []byte
	if resp.Body != nil {
		defer resp.Body.Close()
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}

		// Unmarshal the response body only if the response is ok.
		if resp.StatusCode == http.StatusOK {
			err = json.Unmarshal(body, obj)
		}
	}

	// Returns error if the response is not ok, the response body will be passed if not empty.
	if resp.StatusCode != 200 {
		err = errors.NewGenericServerResponse(
			resp.StatusCode, http.MethodGet,
			schema.GroupResource{Group: AlaudaFeatureGateGroup, Resource: AlaudaFeatureGateResource},
			"", string(body), 0, true)
	}
	return
}

// ObjectToUnstructured converts a runtime.Object to an Unstructured object.
func ObjectToUnstructured(obj runtime.Object) (*unstructured.Unstructured, error) {
	_, ok := obj.(*unstructured.Unstructured)
	if ok {
		return obj.(*unstructured.Unstructured), nil
	}
	unstruct := &unstructured.Unstructured{}
	raw, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	err = unstruct.UnmarshalJSON(raw)
	if err != nil {
		return nil, err
	}
	return unstruct, nil
}

// UnstructuredToObject converts an Unstructured object to the target object.
func UnstructuredToObject(obj *unstructured.Unstructured, target interface{}) error {
	raw, err := obj.MarshalJSON()
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw, target)
	if err != nil {
		return err
	}
	return err
}

// UnstructuredToAlaudaFeatureGate converts "Unstructured" type object to "AlaudaFeatureGate" type.
func UnstructuredToAlaudaFeatureGate(obj *unstructured.Unstructured) (*AlaudaFeatureGate, error) {
	o := &AlaudaFeatureGate{}
	err := UnstructuredToObject(obj, o)
	if err != nil {
		return nil, err
	}
	o.APIVersion = AlaudaFeatureGateAPIVersion
	o.Kind = AlaudaFeatureGateKind
	return o, nil
}

// PodNamespace returns the namespace of the archon pod.
func PodNamespace() string {
	return os.Getenv(EnvPodNamespace)
}
