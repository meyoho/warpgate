package warpgate

import (
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/rest"
	"testing"
)

func TestResolveFeatureGateDependency(t *testing.T) {
	type args struct {
		feature              *AlaudaFeatureGate
		features             map[string]*unstructured.Unstructured
		disabledFeatureStage AlaudaFeatureGateStage
	}
	features := make(map[string]*AlaudaFeatureGate)
	features["a"] = &AlaudaFeatureGate{}
	features["a"].Name = "a"
	features["a"].Spec.Enabled = true
	features["a"].Spec.Description = "feature a"
	features["a"].Spec.Stage = Alpha
	features["a"].Spec.Dependency.FeatureGates = []string{"b", "c"}
	features["a"].Spec.Dependency.Type = AllDenpendencies
	features["b"] = &AlaudaFeatureGate{}
	features["b"].Name = "b"
	features["b"].Spec.Enabled = true
	features["b"].Spec.Dependency.Type = AnyDenpendency
	features["b"].Spec.Dependency.FeatureGates = []string{"c", "d", "nil"}
	features["b"].Spec.Description = "feature b"
	features["b"].Spec.Stage = Beta
	features["c"] = &AlaudaFeatureGate{}
	features["c"].Name = "c"
	features["c"].Spec.Enabled = false
	features["c"].Spec.Description = "feature c"
	features["c"].Spec.Stage = Beta
	features["d"] = &AlaudaFeatureGate{}
	features["d"].Name = "d"
	features["d"].Spec.Enabled = true
	features["d"].Spec.Description = "feature d"
	features["d"].Spec.Stage = Alpha
	features["e"] = &AlaudaFeatureGate{}
	features["e"].Name = "e"
	features["e"].Spec.Enabled = true
	features["e"].Spec.Dependency.Type = AllDenpendencies
	features["e"].Spec.Dependency.FeatureGates = []string{"f"}
	features["e"].Spec.Description = "feature e"
	features["e"].Spec.Stage = Alpha
	features["f"] = &AlaudaFeatureGate{}
	features["f"].Name = "f"
	features["f"].Spec.Enabled = true
	features["f"].Spec.Dependency.Type = AllDenpendencies
	features["f"].Spec.Dependency.FeatureGates = []string{"g"}
	features["f"].Spec.Description = "feature f"
	features["f"].Spec.Stage = Alpha
	features["g"] = &AlaudaFeatureGate{}
	features["g"].Name = "g"
	features["g"].Spec.Enabled = true
	features["g"].Spec.Dependency.Type = AllDenpendencies
	features["g"].Spec.Dependency.FeatureGates = []string{"e"}
	features["g"].Spec.Description = "feature g"
	features["g"].Spec.Stage = Alpha
	features["h"] = &AlaudaFeatureGate{}
	features["h"].Name = "h"
	features["h"].Spec.Enabled = true
	features["h"].Spec.Dependency.Type = AllDenpendencies
	features["h"].Spec.Dependency.FeatureGates = []string{"nil"}
	features["h"].Spec.Description = "feature h"
	features["h"].Spec.Stage = Alpha
	features["i"] = &AlaudaFeatureGate{}
	features["i"].Name = "i"
	features["i"].Spec.Enabled = true
	features["i"].Spec.Dependency.Type = "invalid"
	features["i"].Spec.Dependency.FeatureGates = []string{"a"}
	features["i"].Spec.Description = "feature i"
	features["i"].Spec.Stage = Alpha
	features["beta"] = &AlaudaFeatureGate{}
	features["beta"].Name = BetaFeaturesGateName
	features["beta"].Spec.Enabled = true
	features["beta"].Spec.Description = "global feature Beta"
	features["beta"].Spec.Stage = Beta
	featuresUnstruct := make(map[string]*unstructured.Unstructured)
	for k, v := range features {
		v.Kind = "AlaudaFeatureGate"
		v.APIVersion = "alauda.io/v1"
		f, _ := ObjectToUnstructured(v)
		featuresUnstruct[k] = f
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "all dependency",
			args: args{
				feature:  features["a"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "any dependency",
			args: args{
				feature:  features["b"],
				features: featuresUnstruct,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "loop dependency",
			args: args{
				feature:  features["e"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "Beta feature stage enabled",
			args: args{
				feature:              features["c"],
				disabledFeatureStage: BetaFeaturesGateName,
				features:             featuresUnstruct,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "feature not found with all dep",
			args: args{
				feature:  features["h"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "invalid dep type",
			args: args{
				feature:  features["i"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.disabledFeatureStage != "" {
				unstructured.SetNestedField(
					tt.args.features[string(tt.args.disabledFeatureStage)].Object,
					false, "spec", "enabled")
			}
			got, err := ResolveFeatureGateState(tt.args.feature, tt.args.features, make(map[string]bool), make(map[string]bool))
			if (err != nil) != tt.wantErr {
				t.Errorf("ResolveFeatureGateState() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ResolveFeatureGateState() = %v, want %v", got, tt.want)
			}
		})
	}
}

func fakeInClusterConfigFunc(wantError bool) InClusterConfigFunc {
	return func() (cfg *rest.Config, err error) {
		if wantError {
			err = fmt.Errorf("fake error")
			return
		}
		return &rest.Config{
			Host:        "foo",
			BearerToken: "abcdef",
		}, nil
	}
}

func TestGetFeatureGateDelegator(t *testing.T) {
	tests := []struct {
		name       string
		config     Config
		configFunc InClusterConfigFunc
		wantErr    bool
	}{
		{
			name: "remote ok",
			config: Config{
				APIEndpoint:        "http://archon",
				AuthorizationToken: "acdedf",
			},
			wantErr: false,
		},
		{
			name:       "local ok",
			config:     Config{},
			configFunc: fakeInClusterConfigFunc(false),
			wantErr:    false,
		},
		{
			name:       "local error",
			config:     Config{},
			configFunc: fakeInClusterConfigFunc(true),
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.configFunc != nil {
				inClusterConfig = tt.configFunc
			}
			got, err := GetFeatureGateDelegator(tt.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFeatureGateDelegator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && got == nil {
				t.Errorf("Expect not nil FeatureGateDelegator")
			}
		})
	}
}

func makeAlaudaFeatureGateUnstruct(kind, name, namespace string, enabled bool) *unstructured.Unstructured {
	obj := &unstructured.Unstructured{}
	obj.SetName(name)
	obj.SetNamespace(namespace)
	obj.SetKind(kind)
	obj.SetAPIVersion(AlaudaFeatureGateAPIVersion)
	unstructured.SetNestedField(obj.Object, enabled, "spec", "enabled")
	return obj
}

func TestLocalFeatureGateDelegator_ClusterFeatureGate(t *testing.T) {
	type fields struct {
		clusterFeatureNamespace string
		globalFeatureNamespace  string
	}
	type args struct {
		cluster string
		name    string
		objs    []runtime.Object
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantEnabled bool
		wantErr     bool
	}{
		{
			name: "global enabled",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				cluster: "foo",
				name:    "a",
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
				},
			},
			wantEnabled: true,
		},
		{
			name: "cluster disabled",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				cluster: "foo",
				name:    "a",
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
					makeAlaudaFeatureGateUnstruct(ClusterAlaudaFeatureGateKind, "a", "foo", false),
				},
			},
			wantEnabled: false,
		},
		{
			name: "not found",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				cluster: "foo",
				name:    "c",
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
					makeAlaudaFeatureGateUnstruct(ClusterAlaudaFeatureGateKind, "a", "foo", false),
				},
			},
			wantEnabled: false,
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LocalFeatureGateDelegator{
				clusterFeatureNamespace: tt.fields.clusterFeatureNamespace,
				globalFeatureNamespace:  tt.fields.globalFeatureNamespace,
				client:                  fake.NewSimpleDynamicClient(runtime.NewScheme(), tt.args.objs...),
			}
			got, err := l.ClusterFeatureGate(tt.args.cluster, tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("ClusterFeatureGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && got.Status.Enabled != tt.wantEnabled {
				t.Errorf("Expect %v, got = %v", tt.wantEnabled, got.Status.Enabled)
			}
		})
	}
}

func TestLocalFeatureGateDelegator_ClusterFeatureGates(t *testing.T) {
	type fields struct {
		clusterFeatureNamespace string
		globalFeatureNamespace  string
	}
	type args struct {
		cluster string
		objs    []runtime.Object
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]bool
		wantErr bool
	}{
		{
			name: "ok",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				cluster: "foo",
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "b", "bar", true),
					makeAlaudaFeatureGateUnstruct(ClusterAlaudaFeatureGateKind, "a", "foo", false),
				},
			},
			want: map[string]bool{
				"a": false,
				"b": true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LocalFeatureGateDelegator{
				clusterFeatureNamespace: tt.fields.clusterFeatureNamespace,
				globalFeatureNamespace:  tt.fields.globalFeatureNamespace,
				client:                  fake.NewSimpleDynamicClient(runtime.NewScheme(), tt.args.objs...),
			}
			got, err := l.ClusterFeatureGates(tt.args.cluster)
			if (err != nil) != tt.wantErr {
				t.Errorf("ClusterFeatureGates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				for _, o := range got.Items {
					if tt.want[o.Name] != o.Status.Enabled {
						t.Errorf("Expect %q is %v, got %v", o.Name, tt.want[o.Name], o.Status.Enabled)
						return
					}
				}
			}
		})
	}
}

func TestLocalFeatureGateDelegator_FeatureGate(t *testing.T) {
	type fields struct {
		clusterFeatureNamespace string
		globalFeatureNamespace  string
	}
	type args struct {
		name string
		objs []runtime.Object
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantEnabled bool
		wantErr     bool
	}{
		{
			name: "ok",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				name: "a",
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
					makeAlaudaFeatureGateUnstruct(ClusterAlaudaFeatureGateKind, "a", "foo", false),
				},
			},
			wantEnabled: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LocalFeatureGateDelegator{
				clusterFeatureNamespace: tt.fields.clusterFeatureNamespace,
				globalFeatureNamespace:  tt.fields.globalFeatureNamespace,
				client:                  fake.NewSimpleDynamicClient(runtime.NewScheme(), tt.args.objs...),
			}
			got, err := l.FeatureGate(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("FeatureGate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && got.Status.Enabled != tt.wantEnabled {
				t.Errorf("Expect %v, got = %v", tt.wantEnabled, got.Status.Enabled)
			}
		})
	}
}

func TestLocalFeatureGateDelegator_FeatureGates(t *testing.T) {
	type fields struct {
		clusterFeatureNamespace string
		globalFeatureNamespace  string
	}
	type args struct {
		objs []runtime.Object
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]bool
		wantErr bool
	}{
		{
			name: "ok",
			fields: fields{
				clusterFeatureNamespace: "foo",
				globalFeatureNamespace:  "bar",
			},
			args: args{
				objs: []runtime.Object{
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "a", "bar", true),
					makeAlaudaFeatureGateUnstruct(AlaudaFeatureGateKind, "b", "bar", true),
					makeAlaudaFeatureGateUnstruct(ClusterAlaudaFeatureGateKind, "a", "foo", false),
				},
			},
			want: map[string]bool{
				"a": false,
				"b": true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &LocalFeatureGateDelegator{
				clusterFeatureNamespace: tt.fields.clusterFeatureNamespace,
				globalFeatureNamespace:  tt.fields.globalFeatureNamespace,
				client:                  fake.NewSimpleDynamicClient(runtime.NewScheme(), tt.args.objs...),
			}
			got, err := l.FeatureGates()
			if (err != nil) != tt.wantErr {
				t.Errorf("FeatureGates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				for _, o := range got.Items {
					if tt.want[o.Name] != o.Status.Enabled {
						t.Errorf("Expect %q is %v, got %v", o.Name, tt.want[o.Name], o.Status.Enabled)
						return
					}
				}
			}
		})
	}
}
