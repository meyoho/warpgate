package warpgate

import (
	"fmt"
	"net/http"
	"time"
)

const (
	// DefaultHTTPTimeout is the default HTTP timeout of the http client used in defaultWarpGate.
	DefaultHTTPTimeout = time.Duration(time.Second * 30)
)

// client is a global reusable http client used in defaultWarpGate.
var client = http.Client{
	Timeout: DefaultHTTPTimeout,
}

func init() {
	// Tune transport config
	defaultRoundTripper := http.DefaultTransport
	defaultTransportPointer, ok := defaultRoundTripper.(*http.Transport)
	if !ok {
		panic(fmt.Sprintf("defaultRoundTripper not an *http.Transport"))
	}
	defaultTransport := *defaultTransportPointer
	defaultTransport.MaxIdleConns = 100
	defaultTransport.MaxIdleConnsPerHost = 100
	client.Transport = &defaultTransport
}

// Config defines the configuration of the defaultWarpGate.
type Config struct {
	APIEndpoint        string
	AuthorizationToken string
}

// WarpGate wraps the feature gate API as functions.
type WarpGate interface {
	ListFeatureGates() (fglist *AlaudaFeatureGateList, err error)
	GetFeatureGate(name string) (fg *AlaudaFeatureGate, err error)
	IsFeatureGateEnabled(name string) (enabled bool, err error)
	ListClusterFeatureGates(cluster string) (fglist *AlaudaFeatureGateList, err error)
	GetClusterFeatureGate(cluster, name string) (fg *AlaudaFeatureGate, err error)
	IsClusterFeatureGateEnabled(cluster, name string) (enabled bool, err error)
	IsProductLicensed(product string) (enabled bool)
}

// defaultWarpGate implements the WarpGate interface.
type defaultWarpGate struct {
	Config    Config
	delegator FeatureGateDelegator
}

// NewWarpGate returns a defaultWarpGate by Config.
func NewWarpGate(config Config) (WarpGate, error) {
	dlg, err := GetFeatureGateDelegator(config)
	if err != nil {
		return nil, err
	}
	return &defaultWarpGate{
		Config:    config,
		delegator: dlg,
	}, nil
}

// NewLocalWarpGate returns a local defaultWarpGate without Config.
func NewLocalWarpGate() (WarpGate, error) {
	cfg := Config{}
	dlg, err := GetFeatureGateDelegator(cfg)
	if err != nil {
		return nil, err
	}
	return &defaultWarpGate{
		Config:    cfg,
		delegator: dlg,
	}, nil
}

// ListFeatureGates returns the all global feature gate list.
func (f *defaultWarpGate) ListFeatureGates() (fglist *AlaudaFeatureGateList, err error) {
	return f.delegator.FeatureGates()
}

// GetFeatureGate returns the global feature gate by the name.
func (f *defaultWarpGate) GetFeatureGate(name string) (fg *AlaudaFeatureGate, err error) {
	return f.delegator.FeatureGate(name)
}

// IsFeatureGateEnabled returns if the specified feature gate is enabled.
func (f *defaultWarpGate) IsFeatureGateEnabled(name string) (enabled bool, err error) {
	fg, err := f.GetFeatureGate(name)
	if err != nil {
		return
	}
	enabled = fg.Status.Enabled
	return
}

// ListClusterFeatureGates returns the feature gate list in a cluster.
func (f *defaultWarpGate) ListClusterFeatureGates(cluster string) (fglist *AlaudaFeatureGateList, err error) {
	return f.delegator.ClusterFeatureGates(cluster)
}

// GetClusterFeatureGate returns a feature gate in a cluster by name.
func (f *defaultWarpGate) GetClusterFeatureGate(cluster, name string) (fg *AlaudaFeatureGate, err error) {
	return f.delegator.ClusterFeatureGate(cluster, name)
}

// IsClusterFeatureGateEnabled returns if a feature gate in a cluster is enabled.
func (f *defaultWarpGate) IsClusterFeatureGateEnabled(cluster, name string) (enabled bool, err error) {
	fg, err := f.GetClusterFeatureGate(cluster, name)
	if err != nil {
		return
	}
	enabled = fg.Status.Enabled
	return
}

// IsProductLicensed returns if a product is licensed.
func (f *defaultWarpGate) IsProductLicensed(product string) (enabled bool) {
	url := fmt.Sprintf(LicenseFeatureGateURLTemplate, f.Config.APIEndpoint, product)
	fg := &AlaudaFeatureGate{}
	err := QueryURLForObject(url, f.Config.AuthorizationToken, fg)
	if err != nil {
		return
	}
	return fg.Status.Enabled
}
